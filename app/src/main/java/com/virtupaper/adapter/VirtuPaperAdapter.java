package com.virtupaper.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.virtupaper.magic.AppDataModel;
import com.virtupaper.magic.R;
import com.virtupaper.util.Constants;
import java.util.ArrayList;

/**
 * Created by daw on 20/2/18.
 */

public class VirtuPaperAdapter extends ArrayAdapter<AppDataModel> {

    private ArrayList<AppDataModel> dataSet;
    private Context mContext;
    private static String TAG = "VirtuPaperAdapter";

    public VirtuPaperAdapter(@NonNull Context context, int resource, @NonNull ArrayList<AppDataModel> dataList) {
        super(context, resource, dataList);
        this.dataSet = dataList;
        this.mContext = context;
    }

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView txtPackageName;
        TextView txtVersion;
        TextView txtInstallDate;
        TextView txtUpdateDate;
        ImageView appLogo;
        Button buttonOpen;
        Button buttonRemove;
        Button buttonPlayStore;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        VirtuPaperAdapter.ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new VirtuPaperAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.custom_app, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.textName);
            viewHolder.txtPackageName = (TextView) convertView.findViewById(R.id.textPackageName);
            viewHolder.txtVersion = (TextView) convertView.findViewById(R.id.textVersion);
            viewHolder.txtInstallDate = (TextView) convertView.findViewById(R.id.installDate);
            viewHolder.txtUpdateDate = (TextView) convertView.findViewById(R.id.updateDate);
            viewHolder.appLogo = (ImageView) convertView.findViewById(R.id.imageView);
            viewHolder.buttonOpen = (Button) convertView.findViewById(R.id.open);
            viewHolder.buttonRemove = (Button) convertView.findViewById(R.id.remove);
            viewHolder.buttonPlayStore = (Button) convertView.findViewById(R.id.appLink);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (VirtuPaperAdapter.ViewHolder) convertView.getTag();
        }

        final AppDataModel dataModel = dataSet.get(position);

        viewHolder.txtName.setText(dataModel.appName);
        viewHolder.txtPackageName.setText(dataModel.appPackageName);
        viewHolder.appLogo.setImageDrawable(dataModel.appLogo);
        viewHolder.txtVersion.setText(dataModel.version);
        viewHolder.txtInstallDate.setText("Install: "+dataModel.installDate);
        viewHolder.txtUpdateDate.setText("Update: "+dataModel.lastUpdateDate);
        viewHolder.buttonOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent i = mContext.getPackageManager().getLaunchIntentForPackage(dataModel.appPackageName);
                    mContext.startActivity(i);
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        });

        viewHolder.buttonRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Intent.ACTION_DELETE);
                intent.setData(Uri.parse("package:"+dataModel.appPackageName));
                mContext.startActivity(intent);
            }
        });

        viewHolder.buttonPlayStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(Constants.PLAY_STORE_SUB_LINK+dataModel.appPackageName));
                mContext.startActivity(intent);
            }
        });

        return convertView;
    }
}
