package com.virtupaper.magic;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.provider.SyncStateContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.virtupaper.adapter.VirtuPaperAdapter;
import com.virtupaper.util.Constants;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private ArrayList<AppDataModel> dataList;
    private Context context;
    private ListView listview;
    private TextView textId;
    VirtuPaperAdapter adapter;
    private final int delayUITimer = 2000;
    UIUpdateTimerTask mTaskUI;
    private static String TAG = "MainActivity";
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(getResources().getText(R.string.virtu_paper_apps));
        context = this;
        init();
    }

    private void init() {
        setView();
    }

    private void setView() {
        listview = (ListView) findViewById(R.id.listItem);
        textId = (TextView) findViewById(R.id.textId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Timer uiTimer = new Timer();
        mTaskUI = new UIUpdateTimerTask();
        uiTimer.scheduleAtFixedRate(mTaskUI, 0, delayUITimer);
    }

    public class UIUpdateTimerTask extends TimerTask{
        @Override
        public void run() {
            updateAppList();
        }
    }

    private ArrayList<AppDataModel> getAppMetaData(){
        ArrayList<AppDataModel> data = new ArrayList<AppDataModel>();
        List<PackageInfo> apps = getPackageManager().getInstalledPackages(0);

        for(int i=0;i<apps.size();i++) {
            PackageInfo p = apps.get(i);
            if(p.packageName.contains(Constants.VIRTU_PAPER_SUB_PACKAGE)) {
                if(!p.packageName.equals("com.virtupaper.android.magic")) {
                    AppDataModel newInfo = new AppDataModel();
                    newInfo.appName = p.applicationInfo.loadLabel(getPackageManager()).toString();
                    newInfo.appPackageName = p.packageName;
                    newInfo.appLogo = p.applicationInfo.loadIcon(getPackageManager());
                    newInfo.version = p.versionName+" ("+Integer.toString(p.versionCode)+")";
                    newInfo.installDate = getAppInstallDate(p.firstInstallTime);
                    newInfo.lastUpdateDate = getAppInstallDate(p.lastUpdateTime);
                    data.add(newInfo);
                }
            }
        }
        return data;
    }

    public String getAppInstallDate(long installDate){
        String appInstallDate = null;
        appInstallDate = getDateFormat(installDate);
        return appInstallDate;
    }

    public String getDateFormat(long date){
        String dateValue = null;
        Date d = new Date(date);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateValue = sdf.format(d);
        return dateValue;
    }

    public void updateAppList(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dataList = getAppMetaData();
                sortDataList(dataList);
                uiUpdate();
                if(count != dataList.size()) {
                    count = dataList.size();
                    adapter = new VirtuPaperAdapter(context, R.layout.custom_app, dataList);
                    listview.setAdapter(adapter);
                }
            }
        });
    }

    public void sortDataList(ArrayList<AppDataModel> listData){
        Collections.sort(listData, new Comparator<AppDataModel>()
        {
            @Override
            public int compare(AppDataModel obj1, AppDataModel obj2)
            {
                return obj1.appName.compareToIgnoreCase(obj2.appName);
            }
        });
    }

    public void uiUpdate(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(dataList != null && dataList.size() >0){
                    listview.setVisibility(View.VISIBLE);
                    textId.setVisibility(View.GONE);
                }else {
                    listview.setVisibility(View.GONE);
                    textId.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mTaskUI != null){
            mTaskUI.cancel();
        }
    }
}
