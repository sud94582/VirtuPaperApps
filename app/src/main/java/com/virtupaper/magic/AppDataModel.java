package com.virtupaper.magic;

import android.graphics.drawable.Drawable;

/**
 * Created by daw on 20/2/18.
 */

public class AppDataModel {
    public Drawable appLogo;
    public String appName;
    public String appPackageName;
    public String version;
    public String installDate;
    public String lastUpdateDate;
}
